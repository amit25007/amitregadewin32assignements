class IMultiplication :public IUnknown
{
public:
	//IMultiplication specific method declaration pure virtual
	virtual HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int *) = 0;
};

class IDivision :public IUnknown
{
public:
	//IDivision specific method declarations
	virtual HRESULT __stdcall DivisionOfTwoIntegers(int, int, int *) = 0; //pure virtual
};

//CLSID of MultiplicationDivision component {B39F94E2-2C03-4B01-AB55-98F2E5DA9AA4}
const CLSID CLSID_MultiplicationDivision = {
	0xb39f94e2, 0x2c03, 0x4b01, 0xab, 0x55, 0x98, 0xf2, 0xe5, 0xda, 0x9a, 0xa4
};

//IID of multiplication interface
const IID IID_IMultiplication = {
	0x45154a, 0x7b85, 0x40fd, 0xac, 0x45, 0x27, 0x70, 0x43, 0xae, 0x37, 0xf4
};

//IID of division interface
const IID IID_IDivision = {
	0x84e5b969, 0x9e52, 0x4337, 0x96, 0xe4, 0x6b, 0x76, 0x95, 0xed, 0x6, 0x43
};
