#pragma once
class ISum :public IUnknown
{
public:
	//ISum specific method declarations
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int *) = 0; //pure virtual
};
class ISubtract :public IUnknown
{
public:
	//ISubtract specific method declarations
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int *) = 0; //pure virtual
};

//CLSID of SumSubtract component {A27DAAB6-177D-4C1B-A5D7-508592F7210D}
const CLSID CLSID_SumSubtract = { 0xa27daab6, 0x177d, 0x4c1b, 0xa5, 0xd7, 0x50, 0x85, 0x92, 0xf7, 0x21, 0xd };
//IID of ISum Interface {CAF4B884-DA30-4160-B92D-19D63EF847F1}
const IID IID_ISum = { 0xcdd856ca, 0xd83b, 0x4cc6, 0xa3, 0x7a, 0xd2, 0x4, 0x9f, 0x47, 0x6, 0x7f };
//IID of ISubtract Interface {D89C239B-232A-4EB1-889D-80A043A1E0F0}
const IID IID_ISubtract = { 0xd808f064, 0xb0e0, 0x4e46, 0x8c, 0xfd, 0xd5, 0x46, 0x48, 0x33, 0xaa, 0x5f };
