#pragma once
class ISum :public IUnknown
{
public:
	//ISum specific method declarations
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int *) = 0; //pure virtual
};
class ISubtract :public IUnknown
{
public:
	//ISubtract specific method declarations
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int *) = 0; //pure virtual
};

//CLSID of SumSubtract component {A27DAAB6-177D-4C1B-A5D7-508592F7210D}
const CLSID CLSID_SumSubtract = { 0xa27daab6, 0x177d, 0x4c1b, 0xa5, 0xd7, 0x50, 0x85, 0x92, 0xf7, 0x21, 0xd };
//IID of ISum Interface 
const IID IID_ISum = { 0xcdd856ca, 0xd83b, 0x4cc6, 0xa3, 0x7a, 0xd2, 0x4, 0x9f, 0x47, 0x6, 0x7f };
//IID of ISubtract Interface 
const IID IID_ISubtract = { 0xd808f064, 0xb0e0, 0x4e46, 0x8c, 0xfd, 0xd5, 0x46, 0x48, 0x33, 0xaa, 0x5f };

class IMultiplication :public IUnknown
{
public:
	//IMultiplication specific method declaration pure virtual
	virtual HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int *) = 0;
};

class IDivision :public IUnknown
{
public:
	//IDivision specific method declarations
	virtual HRESULT __stdcall DivisionOfTwoIntegers(int, int, int *) = 0; //pure virtual
};

//CLSID of MultiplicationDivision component {B39F94E2-2C03-4B01-AB55-98F2E5DA9AA4}
const CLSID CLSID_MultiplicationDivision = {0xb39f94e2, 0x2c03, 0x4b01, 0xab, 0x55, 0x98, 0xf2, 0xe5, 0xda, 0x9a, 0xa4};

//IID of multiplication interface
const IID IID_IMultiplication = {0x45154a, 0x7b85, 0x40fd, 0xac, 0x45, 0x27, 0x70, 0x43, 0xae, 0x37, 0xf4};

//IID of division interface
const IID IID_IDivision = {0x84e5b969, 0x9e52, 0x4337, 0x96, 0xe4, 0x6b, 0x76, 0x95, 0xed, 0x6, 0x43};
 