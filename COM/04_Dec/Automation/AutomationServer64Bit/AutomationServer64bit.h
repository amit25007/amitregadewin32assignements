#pragma once
class IMyMath :public IDispatch
{
public:
	//pure virtual
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int *) = 0; //pure virtual
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int *) = 0; //pure virtual
};

//CLSID of MyMath component {644D9C9A-3BF1-458D-90AD-89503CDB88E5}
const CLSID CLSID_MyMath = { 0x644d9c9a, 0x3bf1, 0x458d, 0x90, 0xad, 0x89, 0x50, 0x3c, 0xdb, 0x88, 0xe5 };
//IID of ISum Interface {BB279796-048A-4233-AB9F-2448EE9B4061}
const IID IID_IMyMath = { 0xbb279796, 0x48a, 0x4233, 0xab, 0x9f, 0x24, 0x48, 0xee, 0x9b, 0x40, 0x61 };
