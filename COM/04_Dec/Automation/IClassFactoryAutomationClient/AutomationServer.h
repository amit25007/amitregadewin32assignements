#pragma once
class IMyMath :public IDispatch
{
public:
	//pure virtual
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int *) = 0; //pure virtual
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int *) = 0; //pure virtual
};

//CLSID of IMyMath component {56D754D3-6E5B-4164-89F9-F1ECE9F32194}
const CLSID CLSID_MyMath = { 0x56d754d3, 0x6e5b, 0x4164, 0x89, 0xf9, 0xf1, 0xec, 0xe9, 0xf3, 0x21, 0x94 };
//IID of ISum Interface {768CC9B9-2A6F-476A-A746-9F2704EE3F39}
const IID IID_IMyMath = { 0x768cc9b9, 0x2a6f, 0x476a, 0xa7, 0x46, 0x9f, 0x27, 0x4, 0xee, 0x3f, 0x39 };
