#pragma once
class ISum :public IUnknown
{
public:
	//ISum specific method declarations
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int *) = 0; //pure virtual
};
class ISubtract :public IUnknown
{
public:
	//ISubtract specific method declarations
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int *) = 0; //pure virtual
};

class IMultiplication :public IUnknown
{
public:
	//IMultiplication specific method declarations
	virtual HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int *) = 0; //pure virtual
};
class IDivision :public IUnknown
{
public:
	//IDivision specific method declarations
	virtual HRESULT __stdcall DivisionOfTwoIntegers(int, int, int *) = 0; //pure virtual
};

//CLSID of SumSubtract component {D43AB1CE-8568-4081-8415-A43F01AD94A0}
const CLSID CLSID_SumSubtract = {0xd43ab1ce, 0x8568, 0x4081, 0x84, 0x15, 0xa4, 0x3f, 0x1, 0xad, 0x94, 0xa0};
//IID of ISum Interface {CAF4B884-DA30-4160-B92D-19D63EF847F1}
const IID IID_ISum = {0xcaf4b884, 0xda30, 0x4160, 0xb9, 0x2d, 0x19, 0xd6, 0x3e, 0xf8, 0x47, 0xf1};
//IID of ISubtract Interface {D89C239B-232A-4EB1-889D-80A043A1E0F0}
const IID IID_ISubtract = {0xd89c239b, 0x232a, 0x4eb1, 0x88, 0x9d, 0x80, 0xa0, 0x43, 0xa1, 0xe0, 0xf0};

//IID of ISum Interface {B7B09AD9-D811-4866-9E6F-C3A82230C52A}
const IID IID_IMultiplication = {0xb7b09ad9, 0xd811, 0x4866, 0x9e, 0x6f, 0xc3, 0xa8, 0x22, 0x30, 0xc5, 0x2a};
//IID of ISubtract Interface {5F3E6FD0-F942-4B66-A8E8-814996928355}
const IID IID_IDivision = {0x5f3e6fd0, 0xf942, 0x4b66, 0xa8, 0xe8, 0x81, 0x49, 0x96, 0x92, 0x83, 0x55};
