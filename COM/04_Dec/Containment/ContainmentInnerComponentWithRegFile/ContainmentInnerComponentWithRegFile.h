#pragma once
class IMultiplication :public IUnknown
{
public:
	//IMultiplication specific method declarations
	virtual HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int *) = 0; //pure virtual
};
class IDivision :public IUnknown
{
public:
	//IDivision specific method declarations
	virtual HRESULT __stdcall DivisionOfTwoIntegers(int, int, int *) = 0; //pure virtual
};

//CLSID of SumSubtract component {3CF79E83-26AD-46C6-9A8F-C08AA0740824}
const CLSID CLSID_MultiplicationDivision = {0x3cf79e83, 0x26ad, 0x46c6, 0x9a, 0x8f, 0xc0, 0x8a, 0xa0, 0x74, 0x8, 0x24};
//IID of ISum Interface {B7B09AD9-D811-4866-9E6F-C3A82230C52A}
const IID IID_IMultiplication = {0xb7b09ad9, 0xd811, 0x4866, 0x9e, 0x6f, 0xc3, 0xa8, 0x22, 0x30, 0xc5, 0x2a};
//IID of ISubtract Interface {5F3E6FD0-F942-4B66-A8E8-814996928355}
const IID IID_IDivision = {0x5f3e6fd0, 0xf942, 0x4b66, 0xa8, 0xe8, 0x81, 0x49, 0x96, 0x92, 0x83, 0x55};
