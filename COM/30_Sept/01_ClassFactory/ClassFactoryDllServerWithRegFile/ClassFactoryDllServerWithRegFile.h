#pragma once
class ISum :public IUnknown
{
public:
	//ISum specific method declarations
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int *) = 0; //pure virtual
};
class ISubtract :public IUnknown
{
public:
	//ISubtract specific method declarations
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int *) = 0; //pure virtual
};

//CLSID of SumSubtract component {072E3AED-5604-4B07-AD79-52E8379096B4}
const CLSID CLSID_SumSubtract = {0x72e3aed, 0x5604, 0x4b07, 0xad, 0x79, 0x52, 0xe8, 0x37, 0x90, 0x96, 0xb4};
//IID of ISum Interface {2B8BF6BC-B791-46E2-A1F3-BB748AD48E4D}
const IID IID_ISum = {0x2b8bf6bc, 0xb791, 0x46e2, 0xa1, 0xf3, 0xbb, 0x74, 0x8a, 0xd4, 0x8e, 0x4d};
//IID of ISubtract Interface {8FBE64E1-CE09-402A-9E22-1292FB06619A}
const IID IID_ISubtract = {0x8fbe64e1, 0xce09, 0x402a, 0x9e, 0x22, 0x12, 0x92, 0xfb, 0x6, 0x61, 0x9a};
