﻿
//CSharp Window 

using System;
using System.Windows.Forms;
using System.Drawing;

namespace CSharpWindow
{
    public class CSharpWindow : Form
    {
        public static void Main()
        {
            Application.Run(new CSharpWindow());
        }

        public CSharpWindow()
        {
            Width = 800;
            Height = 600;
            BackColor = Color.White;
            ResizeRedraw = true;
        }
    }
}
