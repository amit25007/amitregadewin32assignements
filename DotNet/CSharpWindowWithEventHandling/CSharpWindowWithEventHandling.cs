﻿//CSharp Window: Key and Mouse Events handled

using System;
using System.Windows.Forms;
using System.Drawing;

namespace CSharpWindowWithHelloWorld
{
    public class CSharpWindowWithHelloWorld : Form
    {
        public static void Main()
        {
            Application.Run(new CSharpWindowWithHelloWorld());
        }

        public CSharpWindowWithHelloWorld()
        {
            Width = 800;
            Height = 600;
            BackColor = Color.White;
            ResizeRedraw = true;

            //Registering Callback to .Net
            // += Add or Overload my call into the code
            this.KeyDown += new KeyEventHandler(MyKeyDown);
            this.MouseDown += new MouseEventHandler(MyMouseDown);
        }

        protected override void OnPaint(PaintEventArgs pea)
        {
            Graphics grfx = pea.Graphics;
            StringFormat strformat = new StringFormat();
            strformat.Alignment = StringAlignment.Center;
            strformat.LineAlignment = StringAlignment.Center;
            grfx.DrawString("Hello World",
                            Font,
                            new SolidBrush(System.Drawing.Color.Green),
                            ClientRectangle,
                            strformat);
        }

        void MyKeyDown(Object sender, KeyEventArgs e)
        {
            MessageBox.Show("key is pressed");
        }

        void MyMouseDown(Object sender, MouseEventArgs e)
        {
            MessageBox.Show("Mouse button is clicked");
        }
    }
}
