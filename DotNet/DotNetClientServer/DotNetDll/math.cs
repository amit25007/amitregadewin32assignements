using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ManagedServerForInterop
{
    [ClassInterface(ClassInterfaceType.AutoDual)]

    public class Math
    { 
        public int MultiplicationValue { get; set; }

        public int DivisionValue { get; set; }

        public Math()
        {
            //empty
        }

        public int MultiplicationOfTwoIntegers(int num1, int num2)
        {
            MultiplicationValue = num1 * num2;
            MessageBox.Show("Multiplication of 2 integers: " + MultiplicationValue);
            return MultiplicationValue;
        }
        public int DivisionOfTwoIntegers(int num1, int num2)
        {
            DivisionValue = num1 / num2;
            MessageBox.Show("Multiplication of 2 integers: " + DivisionValue);
            return DivisionValue;
        }
    }
}
