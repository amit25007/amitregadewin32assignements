#define UNICODE
#include<windows.h>
#include<math.h>
#include"ChemistryClassFactoryServer.h"
// class declaration
class CChemistry :public IChemistry
{
private:
	long m_cRef;
public:
	// constructor method declarations
	CChemistry(void);
	//destructor method declarations
	~CChemistry(void);
	// IUnknown specific method declarations (inherited)
	HRESULT __stdcall QueryInterface(REFIID, void **);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);
	//ISum specific method declarations (inherited)
	HRESULT __stdcall ArrhEquation(double, double, double, double *);
};
class CChemistryClassFactory :public IClassFactory
{
private:
	long m_cRef;
public:
	// constructor method declarations
	CChemistryClassFactory(void);
	//destructor method declarations
	~CChemistryClassFactory(void);
	// IUnknown specific method declarations (inherited)
	HRESULT __stdcall QueryInterface(REFIID, void **);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);
	//IClassFactory specific method declarations (inherited)
	HRESULT __stdcall CreateInstance(IUnknown *, REFIID, void **);
	HRESULT __stdcall LockServer(BOOL);
};

// global variable declaration
long glNumberofActiveComponent = 0;// number of active components
long glNumberofServerLocks = 0; //number of locks on this dll

//Dll main
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD dwReason, LPVOID Reserve)
{
	//code
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	}
	return(TRUE);
}

//Implementation of CChemistry's constructor method
CChemistry::CChemistry(void)
{
	//code 
	m_cRef = 1; //hardcoded initialization to anticipate possible failure of QueryInterface()
	InterlockedIncrement(&glNumberofActiveComponent); //increment global counter
}
//Implementation of CChemistry's destructor method
CChemistry::~CChemistry(void)
{
	//code 
	InterlockedDecrement(&glNumberofActiveComponent); //decrement global counter
}
//Implementation of CChemistry's IUnknown's method
HRESULT CChemistry::QueryInterface(REFIID riid, void **ppv)
{
	// code
	if (riid == IID_IUnknown)
		*ppv = static_cast<IChemistry *>(this);
	else if (riid == IID_IChemistry)
		*ppv = static_cast<IChemistry *>(this);
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}
	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return(S_OK);
}
ULONG CChemistry::AddRef(void)
{
	//code
	InterlockedIncrement(&m_cRef);
	return(m_cRef);
}
ULONG CChemistry::Release(void)
{
	//code
	InterlockedDecrement(&m_cRef);
	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}
	return(m_cRef);
}
//Implementation of ISum methods
HRESULT CChemistry::ArrhEquation(double ActivationEnergy, double UniversalGasConstant, 
									double AbsuluteTemp, double *pRateConstant)
{
	//code
	*pRateConstant = exp( -ActivationEnergy /(UniversalGasConstant*AbsuluteTemp));
	return(S_OK);
}

//Implementation of CChemistryClassFactory's constructor method
CChemistryClassFactory::CChemistryClassFactory(void)
{
	//code
	m_cRef = 1; //hardcoded initiazation to anticipate possible failure of QueryInterface()
}
//Implementation of CChemistryClassFactory's destructor method
CChemistryClassFactory::~CChemistryClassFactory(void)
{
	//code
}

//Implementation of CChemistryClassFactory's IClassFactory's IUnknown's method
HRESULT CChemistryClassFactory::QueryInterface(REFIID riid, void **ppv)
{
	//code
	//MessageBox(NULL, TEXT("In QueryInterface"), TEXT("INFO"), MB_OK);
	if (riid == IID_IUnknown)
		*ppv = static_cast<IClassFactory *>(this);
	else if (riid == IID_IClassFactory)
		*ppv = static_cast<IClassFactory *>(this);
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}
	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return(S_OK);
}

ULONG CChemistryClassFactory::AddRef(void)
{
	//code
	InterlockedIncrement(&m_cRef);
	return(m_cRef);
}
ULONG CChemistryClassFactory::Release(void)
{
	//code
	InterlockedDecrement(&m_cRef);
	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}
	return(m_cRef);
}

//Implementation of CSumSutractClassFactory's methods
HRESULT CChemistryClassFactory::CreateInstance(IUnknown *pUnkOuter, REFIID riid, void **ppv)
{
	//variable declaration
	CChemistry *pCChemistry = NULL;
	HRESULT hr;
	//code
	if (pUnkOuter != NULL)
	{
		return(CLASS_E_NOAGGREGATION);
	}
	//create the instance of component i.e. of CChemistry class
	pCChemistry = new CChemistry;
	if (pCChemistry == NULL)
		return(E_OUTOFMEMORY);
	//get the requested interface
	hr = pCChemistry->QueryInterface(riid, ppv);
	pCChemistry->Release(); //anticipate possible faiure of QueryInterface
	return(hr);
}

HRESULT CChemistryClassFactory::LockServer(BOOL fLock)
{
	//code
	if (fLock)
		InterlockedIncrement(&glNumberofServerLocks);
	else
		InterlockedDecrement(&glNumberofServerLocks);
	return(S_OK);
}

//Implementation of exported functions from this dll
HRESULT __stdcall DllGetClassObject(REFCLSID rclsid, REFIID riid, void **ppv)
{
	//variable declarations
	//MessageBox(NULL, TEXT("In get class object"), TEXT("INFO"), MB_OK);
	CChemistryClassFactory *pCChemistryClassFactory = NULL;
	HRESULT hr;
	//code
	if (rclsid != CLSID_Chemistry)
		return(CLASS_E_CLASSNOTAVAILABLE);
	//create class factory
	pCChemistryClassFactory = new CChemistryClassFactory;
	if (pCChemistryClassFactory == NULL)
		return(E_OUTOFMEMORY);
	hr = pCChemistryClassFactory->QueryInterface(riid, ppv);
	pCChemistryClassFactory->Release();//anticipate possible failure of QueryInterface
	return(hr);
}

HRESULT __stdcall DllCanUnloadNow(void)
{
	//code
	if ((glNumberofActiveComponent == 0) && (glNumberofServerLocks == 0))
		return(S_OK);
	else
		return(S_FALSE);
}
