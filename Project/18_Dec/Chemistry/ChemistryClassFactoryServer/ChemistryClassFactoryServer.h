class IChemistry :public IUnknown
{
public:
	//ArrhEquation specific method declarations
	virtual HRESULT __stdcall ArrhEquation(double, double, double, double *) = 0; //pure virtual
};

//CLSID of SumSubtract component {66FE4F97-B3C7-4A12-BCED-F8AA15169F25}
const CLSID CLSID_Chemistry = {0x66fe4f97, 0xb3c7, 0x4a12, 0xbc, 0xed, 0xf8, 0xaa, 0x15, 0x16, 0x9f, 0x25};
//IID of ISum Interface {92CCF54E-4535-4884-A056-0E61893D15DE}
const IID IID_IChemistry = {0x92ccf54e, 0x4535, 0x4884, 0xa0, 0x56, 0xe, 0x61, 0x89, 0x3d, 0x15, 0xde};
