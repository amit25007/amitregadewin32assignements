// Headers
#define UNICODE
#include <windows.h>
#include"ChemistryClassFactoryServer.h"

// function declaration
void SafeInterfaceRelease(void);

//global variables
HRESULT hr;
IChemistry *pIChemistry = NULL;
double ActivationEnergy, UniversalGasConstant, AbsuluteTemp, iRateConstant;

BOOL WINAPI DllMain(HMODULE hDll, DWORD dwReason, LPVOID lpReserved)
{
	
	//code
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		//COM initialization
		hr = CoInitialize(NULL);
		if (FAILED(hr))
		{
			MessageBox(NULL, TEXT("COM Library can not be intialized.\nProgram will now exit."), TEXT("Program Error"), MB_OK);
			return true;
		}
		hr = CoCreateInstance(CLSID_Chemistry, NULL, CLSCTX_INPROC_SERVER, IID_IChemistry, (void **)&pIChemistry);
		if (FAILED(hr))
		{
			MessageBox(NULL, TEXT("IChemistry Interface can not be obtained"), TEXT("Error"), MB_OK);
			return true; 
		}
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		//as IChemistry is now needed onwords, release it
		pIChemistry->Release();
		pIChemistry = NULL; // make released interface NULL
		//COM Un-initialization
		CoUninitialize();
		break;
	}
	return true;
}

//code using module defination file
extern "C" double ClientArrhEquation(double ActivationEnergy, double UniversalGasConstant, double AbsuluteTemp)
{
	pIChemistry->ArrhEquation(ActivationEnergy, UniversalGasConstant, AbsuluteTemp, &iRateConstant);
	return(iRateConstant);
}

void SafeInterfaceRelease(void)
{
	//code 
	if (pIChemistry)
	{
		pIChemistry->Release();
		pIChemistry = NULL;
	}
}