using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ManagedServerForInterop
{
    [ClassInterface(ClassInterfaceType.AutoDual)]

    public class MathProject
    { 
        public double ConeVolumeValue { get; set; }

        public MathProject()
        {
            //empty
        }

        public double ConeVolume(int radius, int height)
        {
            ConeVolumeValue = (3.1415 * radius * radius * height) / 3;
            return ConeVolumeValue;
        }
    }
}
