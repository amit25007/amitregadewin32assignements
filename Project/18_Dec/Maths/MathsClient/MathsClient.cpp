#import"MathDotNetServerDll.tlb" no_namespace, raw_interfaces_only
#include "MathDotNetServerDll.tlh"

#include<Windows.h>
#include<stdio.h>
#include"MathsClient.h"

//fun declaration
void ComErrorDescriptionString(HWND, HRESULT);

//global variables
HRESULT hr;
CLSID clsidMath;
_MathProject *app;

//DllMain
BOOL WINAPI DllMain(HMODULE hDll, DWORD dwReason, LPVOID lpReserved)
{
	//code
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	}
	return true;
}

//code using module defination file
extern "C" void Initialize()
{
	//COM initialization
	hr = CoInitialize(NULL);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("COM Library can not be intialized.\nProgram will now exit."), TEXT("Program Error"), MB_OK);
	}
	//_MathProject *app;
	hr = CLSIDFromProgID(L"ManagedServerForInterop.MathProject", &clsidMath);

	if (FAILED(hr))
	{
		ComErrorDescriptionString(NULL, hr);
	}

	hr = CoCreateInstance(clsidMath, NULL, CLSCTX_INPROC_SERVER, __uuidof(_MathProject), (VOID **)&app);
	if (FAILED(hr))
	{
		ComErrorDescriptionString(NULL, hr);
	}
	
}

extern "C" double CalculateConeVolume(int radius, int height)
{
	TCHAR str[255];
	int iNum1, iNum2;
	double iVolume;
	
	hr = app->ConeVolume(radius, height, &iVolume);
	if (FAILED(hr))
	{
		ComErrorDescriptionString(NULL, hr);
	}
	return(iVolume);
}

extern "C" void Uninitialize()
{
	app = NULL;
	CoUninitialize();
}


void ComErrorDescriptionString(HWND hwnd, HRESULT hr)
{
	//variable declaration
	TCHAR* szErrorMessage = NULL;
	TCHAR str[255];

	if (FACILITY_WINDOWS == HRESULT_FACILITY(hr))
	{
		hr = HRESULT_CODE(hr);
	}

	if (FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&szErrorMessage, 0, NULL) != 0)
	{
		wsprintf(str, TEXT("%s"), szErrorMessage);
		LocalFree(szErrorMessage);
	}
	else
		wsprintf(str, TEXT("[Could not find a description for error # %#x.]\n"), hr);

	MessageBox(hwnd, str, TEXT("Com error"), MB_OK);
}








