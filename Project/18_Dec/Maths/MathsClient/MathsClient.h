#pragma once
extern "C" void Initialize();
extern "C" double CalculateConeVolume(int, int);
extern "C" void Uninitialize();
