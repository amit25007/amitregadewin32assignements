#include <windows.h>
#include <math.h>
#include <stdlib.h>
#include <tchar.h>
#include <stdio.h>
#include <strsafe.h>
#include <malloc.h>
#include "ProjectPCMB.h"
#include <wtypes.h>

#define ACTIVE_MODULE_PHYSICS 1
#define ACTIVE_MODULE_CHEMISTRY 2
#define ACTIVE_MODULE_MATHS 3
#define startX 150
#define startY 60
#define width 1200
#define height 700


//window variables
//UINT width, height, startX, startY, 
UINT active_module;
HWND hwndmodeless = NULL;
/*startX = 150;
startY = 60;
width = 1200;
height = 700;*/

// global functions declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK DlgProc(HWND, UINT, WPARAM, LPARAM);
BOOL ToggleRadioButton(WPARAM);


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
  
	

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE("f1icon"));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register above class
	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(szAppName, TEXT("My Application"), WS_OVERLAPPEDWINDOW, startX, startY, width, height, NULL, NULL, hInstance, NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (hwndmodeless == NULL || (IsDialogMessage(hwndmodeless, &msg) == 0))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	return static_cast<int>(msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, hMemdc;
	static HBITMAP hBitmap;
	PAINTSTRUCT ps;
	BITMAP bmp;
	HINSTANCE hInst = NULL;
	RECT rect, wRect;
	HFONT hFont;

	//screen variables
	RECT desktop;
	const HWND hDesktop = GetDesktopWindow();
	GetWindowRect(hDesktop, &desktop);
	int ScreenWidth = desktop.right; //window.screen.width;
	int ScreenHeight = desktop.bottom;  //window.screen.height;
	TCHAR str[255];
	switch (msg)
	{
	case WM_CREATE:
		hInst = (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE);
		hBitmap = LoadBitmap(((LPCREATESTRUCT)lParam)->hInstance, MAKEINTRESOURCE(F1_BITMAP));
		break;
	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);
		hMemdc = CreateCompatibleDC(hdc);
		SelectObject(hMemdc, hBitmap);
		GetObject(hBitmap, sizeof(BITMAP), &bmp);
		StretchBlt(hdc, 0, 0, ScreenWidth, ScreenHeight, hMemdc, 0, 0, width, height, SRCCOPY);
		SelectObject(hMemdc, hBitmap);

		GetWindowRect(hwnd, &wRect);
		hFont = CreateFont(28, 0, 0, 0, FW_SEMIBOLD, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_OUTLINE_PRECIS,
		CLIP_DEFAULT_PRECIS, CLEARTYPE_QUALITY, VARIABLE_PITCH, TEXT("Bookman"));
		SelectObject(hdc, hFont);
		SetRect(&rect, 20, 20, 100, 100);
		SetBkMode(hdc, TRANSPARENT);
		SetTextColor(hdc, RGB(232, 242, 26));
		DrawText(hdc, TEXT("Amit Regade"), -1, &rect, DT_NOCLIP);
		SetRect(&rect, 20, 60, 100, 100);
		DrawText(hdc, TEXT("WinRT 2018 WM_TOUCH"), -1, &rect, DT_NOCLIP);
		SetRect(&rect, wRect.right-wRect.left-250, 30, 100, 100);
		DrawText(hdc, TEXT("AstroMediComp"), -1, &rect, DT_NOCLIP);
		DeleteDC(hMemdc);
		EndPaint(hwnd, &ps);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_SPACE:
			hwndmodeless = CreateDialog(hInst, "MyDialog", hwnd, DlgProc);
			ToggleRadioButton(NULL);
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(WM_QUIT);
		break;
	default:
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}
	return 0;
}

BOOL CALLBACK DlgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//dll var
	static HMODULE hlib = NULL;
	typedef double(*pfnLuminosityDistance)(double, double);
	pfnLuminosityDistance pfnP = NULL;
	typedef double(*pfnRateConstant)(double, double, double);
	pfnRateConstant pfnC = NULL;
	typedef void(*pfnInitializeMath)();
	pfnInitializeMath pfnM1 = NULL;
	typedef double(*pfnConeVolume)(int, int);
	pfnConeVolume pfnM2 = NULL;
	typedef void(*pfnUninitializeMath)();
	pfnUninitializeMath pfnM3 = NULL;

	HWND hflux, hluminisoty, hdistance, hPhysics, hCalculateLuminosity, hResetPhysics, hResetChemistry, hResetMaths;

	double static Luminosity = 0.0, Flux = 0.0, LuminosityDistance = 0.0;
	double ActivationEnergy, UniGasConstant, AbsuluteTemp, RateConstant;
	double Radius, Height, ConeVolume;
	TCHAR static sLuminosity[255], sFlux[255], sLuminosityDistance[255];
	TCHAR static sActivationEnergy[255], sUniGasConstant[255], sAbsuluteTemp[255], sRateConstant[255];
	TCHAR static sRadius[255], sHeight[255], sConeVolume[255];
	hflux = GetDlgItem(hwndmodeless, ID_FLUX);
	hluminisoty = GetDlgItem(hwndmodeless, ID_LUMINOSITY);
	hdistance = GetDlgItem(hwndmodeless, ID_DISTANCE);
	hPhysics = GetDlgItem(hwndmodeless, ID_LUMINOSITYDISTANCETEXT);
	hCalculateLuminosity= GetDlgItem(hwndmodeless, ID_CALCULATE_LUMINOSITY);
	hResetPhysics = GetDlgItem(hwndmodeless, ID_RESETPHYSICS);

	HWND hActivationEnergy, hUniGasConstant, hAbsuluteTemp, hRateConstant, hCalculateRateConstant;
	//hExponentialFactor = GetDlgItem(hwndmodeless, ID_EXPONENTIALFACTOR);
	hActivationEnergy = GetDlgItem(hwndmodeless, ID_ACTIVATIONENERGY);
	hUniGasConstant = GetDlgItem(hwndmodeless, ID_UNIGASCONSTANT);
	hAbsuluteTemp = GetDlgItem(hwndmodeless, ID_ABSULUTETEMP);
	hRateConstant = GetDlgItem(hwndmodeless, ID_RATECONSTANT);
	hCalculateRateConstant = GetDlgItem(hwndmodeless, ID_CALCULATE_RATE_CONSTANT);

	hResetChemistry = GetDlgItem(hwndmodeless, ID_RESETCHEMISTRY);
	hResetMaths = GetDlgItem(hwndmodeless, ID_RESETMATHS);

	//file io variables
	FILE *pFile=NULL;
	char str[255];

	switch (msg)
	{
	case WM_INITDIALOG:
		return(TRUE);
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case ID_PHYSICS:
			active_module = ACTIVE_MODULE_PHYSICS;
			ToggleRadioButton(wParam);
			FreeLibrary(hlib);
			hlib = LoadLibrary(TEXT("LuminosityDLL.dll"));
			if (hlib == NULL)
			{
				MessageBox(hwndmodeless, TEXT("dll failed to load"), TEXT("Error"), MB_OK);
			}
			break;
		case ID_CHEMISTRY:
			active_module = ACTIVE_MODULE_CHEMISTRY;
			ToggleRadioButton(wParam);
			FreeLibrary(hlib);
			hlib = LoadLibrary(TEXT("ClientChemistry.dll"));
			if (hlib == NULL)
			{
				MessageBox(hwndmodeless, TEXT("dll failed to load"), TEXT("Error"), MB_OK);
			}
			break;
		case ID_MATHS:
			active_module = ACTIVE_MODULE_MATHS;
			ToggleRadioButton(wParam);
			FreeLibrary(hlib);
			hlib = LoadLibrary(TEXT("MathsClient.dll"));
			if (hlib == NULL)
			{
				MessageBox(hwndmodeless, TEXT("dll failed to load"), TEXT("Error"), MB_OK);
			}
			pfnM1 = (pfnInitializeMath)GetProcAddress(hlib, "Initialize");
			pfnM1();
			break;
		case ID_INCREAMENT_LUMINOSITY:
			GetDlgItemText(hwndmodeless, ID_LUMINOSITY, sLuminosity, 255);
			Luminosity = atof(sLuminosity);
			Luminosity = Luminosity + 1.0;
			_gcvt_s(sLuminosity, Luminosity, 10);
			SetDlgItemText(hwndmodeless, ID_LUMINOSITY, sLuminosity);
			break;
		case ID_DECREAMENT_LUMINOSITY:
			GetDlgItemText(hwndmodeless, ID_LUMINOSITY, sLuminosity, 255);
			Luminosity = atof(sLuminosity);
			Luminosity = Luminosity - 1.0;
			_gcvt_s(sLuminosity, Luminosity, 10);
			SetDlgItemText(hwndmodeless, ID_LUMINOSITY, sLuminosity);
			break;
		case ID_INCREAMENT_FLUX:
			GetDlgItemText(hwndmodeless, ID_FLUX, sFlux, 255);
			Flux = atof(sFlux);
			Flux = Flux + 1.0;
			_gcvt_s(sFlux, Flux, 10);
			SetDlgItemText(hwndmodeless, ID_FLUX, sFlux);
			break;
		case ID_DECREAMENT_FLUX:
			GetDlgItemText(hwndmodeless, ID_FLUX, sFlux, 255);
			Flux = atof(sFlux);
			Flux = Flux - 1.0;
			_gcvt_s(sFlux, Flux, 10);
			SetDlgItemText(hwndmodeless, ID_FLUX, sFlux);
			break;
		case ID_CALCULATE_LUMINOSITY:
			GetDlgItemText(hwndmodeless, ID_FLUX, sFlux, 255);
			GetDlgItemText(hwndmodeless, ID_LUMINOSITY, sLuminosity, 255);
			Flux = atof(sFlux);
			Luminosity = atof(sLuminosity);
			if (Flux <= 0 || Luminosity < 0)
			{
				SetDlgItemText(hwndmodeless, ID_DISTANCE, NULL);
				MessageBox(hwndmodeless, TEXT("Please enter positive integer values"), TEXT("Error"), MB_OK);
			}
			else
			{
				// to find the address of required fun
				pfnP = (pfnLuminosityDistance)GetProcAddress(hlib, "LuminosityDistance");
				LuminosityDistance = pfnP(Luminosity, Flux);
				_gcvt_s(sLuminosityDistance, LuminosityDistance, 10);
				SetDlgItemText(hwndmodeless, ID_DISTANCE, sLuminosityDistance);			
			}
			break;
		case ID_SAVE_TO_FILEP:
		case ID_SAVE_TO_FILEC:
		case ID_SAVE_TO_FILEM:
			switch (active_module)
			{
			case ACTIVE_MODULE_PHYSICS:
				fopen_s(&pFile, "physics.txt", "a");
				wsprintf(str, "Flux:%s, Luminosity: %s, Luminosity Distance: %s\n", sFlux, sLuminosity, sLuminosityDistance);
				break;
			case ACTIVE_MODULE_CHEMISTRY:
				fopen_s(&pFile, "chemistry.txt", "a");
				wsprintf(str, "ActivationEnergy:%s, UniGasConstant:%s, AbsuluteTempRate:%s, Rate constant: %s\n", sActivationEnergy, sUniGasConstant, sAbsuluteTemp, sRateConstant);
				break;
			case ACTIVE_MODULE_MATHS:
				fopen_s(&pFile, "maths.txt", "a");
				wsprintf(str, "Radius:%s, Height:%s, Cone volume: %s\n", sRadius, sHeight, sConeVolume);
				break;
			default:
				break;
			}
			fprintf_s(pFile, str);
			fclose(pFile);
			pFile = NULL;
			break;
		case ID_CALCULATE_RATE_CONSTANT:
			//GetDlgItemText(hwndmodeless, ID_EXPONENTIALFACTOR, sExponentialFactor, 255);
			GetDlgItemText(hwndmodeless, ID_ACTIVATIONENERGY, sActivationEnergy, 255);
			GetDlgItemText(hwndmodeless, ID_UNIGASCONSTANT, sUniGasConstant, 255);
			GetDlgItemText(hwndmodeless, ID_ABSULUTETEMP, sAbsuluteTemp, 255);
			//ExponentialFactor = atof(sExponentialFactor);
			ActivationEnergy = atof(sActivationEnergy);
			UniGasConstant = atof(sUniGasConstant);
			AbsuluteTemp = atof(sAbsuluteTemp);
			if (ActivationEnergy <= 0 || UniGasConstant <= 0 || AbsuluteTemp <= 0)
			{
				SetDlgItemText(hwndmodeless, ID_RATECONSTANT, NULL);
				MessageBox(hwndmodeless, TEXT("Please enter positive integer values"), TEXT("Error"), MB_OK);
			}
			else
			{
				// to find the address of required fun
				pfnC = (pfnRateConstant)GetProcAddress(hlib, "ClientArrhEquation");
				RateConstant = pfnC(ActivationEnergy, UniGasConstant, AbsuluteTemp);
				_gcvt_s(sRateConstant, RateConstant, 10);
				SetDlgItemText(hwndmodeless, ID_RATECONSTANT, sRateConstant);
			}
			break;
		case ID_CALCULATE_CONE_VOLUME:
			GetDlgItemText(hwndmodeless, ID_RADIUS, sRadius, 255);
			GetDlgItemText(hwndmodeless, ID_HEIGHT, sHeight, 255);
			Radius = atof(sRadius);
			Height = atof(sHeight);
			if (Radius <= 0 || Height <= 0)
			{
				SetDlgItemText(hwndmodeless, ID_CONEVOLUME, NULL);
				MessageBox(hwndmodeless, TEXT("Please enter positive integer values"), TEXT("Error"), MB_OK);
			}
			else
			{
				// to find the address of required fun
				pfnM2 = (pfnConeVolume)GetProcAddress(hlib, "CalculateConeVolume");
				ConeVolume = pfnM2(Radius, Height);
				_gcvt_s(sConeVolume, ConeVolume, 10);
				SetDlgItemText(hwndmodeless, ID_CONEVOLUME, sConeVolume);
				pfnM3 = (pfnUninitializeMath)GetProcAddress(hlib, "Uninitialize");
				pfnM3();
			}
			break;
		case ID_RESETPHYSICS:
			SetDlgItemText(hwndmodeless, ID_FLUX, NULL);
			SetDlgItemText(hwndmodeless, ID_LUMINOSITY, NULL);
			SetDlgItemText(hwndmodeless, ID_DISTANCE, NULL);
			break;
		case ID_RESETCHEMISTRY:
			SetDlgItemText(hwndmodeless, ID_ACTIVATIONENERGY, NULL);
			SetDlgItemText(hwndmodeless, ID_UNIGASCONSTANT, NULL);
			SetDlgItemText(hwndmodeless, ID_ABSULUTETEMP, NULL);
			SetDlgItemText(hwndmodeless, ID_RATECONSTANT, NULL);
			break;
		case ID_RESETMATHS:
			SetDlgItemText(hwndmodeless, ID_RADIUS, NULL);
			SetDlgItemText(hwndmodeless, ID_HEIGHT, NULL);
			SetDlgItemText(hwndmodeless, ID_CONEVOLUME, NULL);
			break;
		case ID_EXIT:
			EndDialog(hwnd, 0);
			break;
		}
		return(TRUE);
	}
	return(FALSE);
}

BOOL ToggleRadioButton(WPARAM RadioButton)
{
	HWND hflux, hluminisoty, hDistance, hPhysics, hCalculateLuminosity, hResetPhysics, hSaveState, hSaveStateP, hIncreamentF, hDecreamentF, hIncreamentL, hDecreamentL;
	hSaveStateP = GetDlgItem(hwndmodeless, ID_SAVE_TO_FILEP);
	hflux = GetDlgItem(hwndmodeless, ID_FLUX);
	hluminisoty = GetDlgItem(hwndmodeless, ID_LUMINOSITY);
	hIncreamentF = GetDlgItem(hwndmodeless, ID_INCREAMENT_FLUX);
	hDecreamentF = GetDlgItem(hwndmodeless, ID_DECREAMENT_FLUX);
	hIncreamentL = GetDlgItem(hwndmodeless, ID_INCREAMENT_LUMINOSITY);
	hDecreamentL = GetDlgItem(hwndmodeless, ID_DECREAMENT_LUMINOSITY);
	hDistance = GetDlgItem(hwndmodeless, ID_DISTANCE);
	hPhysics = GetDlgItem(hwndmodeless, ID_LUMINOSITYDISTANCETEXT);
	hCalculateLuminosity= GetDlgItem(hwndmodeless, ID_CALCULATE_LUMINOSITY);
	hResetPhysics = GetDlgItem(hwndmodeless, ID_RESETPHYSICS);

	HWND hChemistry, hActivationEnergy, hUniGasConstant, hAbsuluteTemp, hRateConstant, hCalculateRateConstant, hResetChemistry, hSaveStateC;
	hChemistry = GetDlgItem(hwndmodeless, ID_ARRHENIUSEQUATIONTEXT);
	hSaveStateC = GetDlgItem(hwndmodeless, ID_SAVE_TO_FILEC);
	hActivationEnergy = GetDlgItem(hwndmodeless, ID_ACTIVATIONENERGY);
	hUniGasConstant = GetDlgItem(hwndmodeless, ID_UNIGASCONSTANT);
	hAbsuluteTemp = GetDlgItem(hwndmodeless, ID_ABSULUTETEMP);
	hRateConstant = GetDlgItem(hwndmodeless, ID_RATECONSTANT);
	hCalculateRateConstant = GetDlgItem(hwndmodeless, ID_CALCULATE_RATE_CONSTANT);
	hResetChemistry = GetDlgItem(hwndmodeless, ID_RESETCHEMISTRY);

	HWND hMaths, hRadius, hHeight, hCaculateConeVolume, hConeVolume, hResetMaths, hSaveStateM;
	hMaths = GetDlgItem(hwndmodeless, ID_MATHSEQUATIONTEXT);
	hSaveStateM = GetDlgItem(hwndmodeless, ID_SAVE_TO_FILEM);
	hRadius = GetDlgItem(hwndmodeless, ID_RADIUS);
	hHeight = GetDlgItem(hwndmodeless, ID_HEIGHT);
	hCaculateConeVolume = GetDlgItem(hwndmodeless, ID_CALCULATE_CONE_VOLUME);
	hConeVolume = GetDlgItem(hwndmodeless, ID_CONEVOLUME);
	hResetMaths = GetDlgItem(hwndmodeless, ID_RESETMATHS);

	switch (RadioButton)
	{
	case ID_PHYSICS:
		EnableWindow(hChemistry, TRUE);
		EnableWindow(hActivationEnergy, FALSE);
		EnableWindow(hUniGasConstant, FALSE);
		EnableWindow(hAbsuluteTemp, FALSE);
		EnableWindow(hRateConstant, FALSE);
		EnableWindow(hCalculateRateConstant, FALSE);
		EnableWindow(hResetChemistry, FALSE);
		EnableWindow(hSaveStateC, FALSE);
		EnableWindow(hRadius, FALSE);
		EnableWindow(hHeight, FALSE);
		EnableWindow(hCaculateConeVolume, FALSE);
		EnableWindow(hConeVolume, FALSE);
		EnableWindow(hResetMaths, FALSE);
		EnableWindow(hSaveStateM, FALSE);
		EnableWindow(hflux, TRUE);
		EnableWindow(hluminisoty, TRUE);
		EnableWindow(hDistance, TRUE);
		EnableWindow(hPhysics, TRUE);
		EnableWindow(hCalculateLuminosity, TRUE);
		EnableWindow(hResetPhysics, TRUE);
		EnableWindow(hSaveStateP, TRUE);
		EnableWindow(hIncreamentF, TRUE);
		EnableWindow(hDecreamentF, TRUE);
		EnableWindow(hIncreamentL, TRUE);
		EnableWindow(hDecreamentL, TRUE);
		break;
	case ID_CHEMISTRY:
		EnableWindow(hPhysics, FALSE);
		EnableWindow(hflux, FALSE);
		EnableWindow(hluminisoty, FALSE);
		EnableWindow(hIncreamentF, FALSE);
		EnableWindow(hDecreamentF, FALSE);
		EnableWindow(hIncreamentL, FALSE);
		EnableWindow(hDecreamentL, FALSE);
		EnableWindow(hDistance, FALSE);
		EnableWindow(hCalculateLuminosity, FALSE);
		EnableWindow(hResetPhysics, FALSE);
		EnableWindow(hSaveStateP, FALSE);
		EnableWindow(hRadius, FALSE);
		EnableWindow(hHeight, FALSE);
		EnableWindow(hCaculateConeVolume, FALSE);
		EnableWindow(hConeVolume, FALSE);
		EnableWindow(hResetMaths, FALSE);
		EnableWindow(hSaveStateM, FALSE);
		EnableWindow(hActivationEnergy, TRUE);
		EnableWindow(hUniGasConstant, TRUE);
		EnableWindow(hAbsuluteTemp, TRUE);
		EnableWindow(hRateConstant, TRUE);
		EnableWindow(hCalculateRateConstant, TRUE);
		EnableWindow(hResetChemistry, TRUE);
		EnableWindow(hSaveStateC, TRUE);
		break;
	case ID_MATHS:
		EnableWindow(hflux, FALSE);
		EnableWindow(hluminisoty, FALSE);
		EnableWindow(hDistance, FALSE);
		EnableWindow(hIncreamentF, FALSE);
		EnableWindow(hDecreamentF, FALSE);
		EnableWindow(hIncreamentL, FALSE);
		EnableWindow(hDecreamentL, FALSE);
		EnableWindow(hPhysics, FALSE);
		EnableWindow(hCalculateLuminosity, FALSE);
		EnableWindow(hResetPhysics, FALSE);
		EnableWindow(hSaveStateP, FALSE);
		EnableWindow(hActivationEnergy, FALSE);
		EnableWindow(hUniGasConstant, FALSE);
		EnableWindow(hAbsuluteTemp, FALSE);
		EnableWindow(hRateConstant, FALSE);
		EnableWindow(hCalculateRateConstant, FALSE);
		EnableWindow(hResetChemistry, FALSE);
		EnableWindow(hSaveStateC, FALSE);
		EnableWindow(hRadius, TRUE);
		EnableWindow(hHeight, TRUE);
		EnableWindow(hCaculateConeVolume, TRUE);
		EnableWindow(hConeVolume, TRUE);
		EnableWindow(hResetMaths, TRUE);
		EnableWindow(hSaveStateM, TRUE);
		break;
	case NULL:
		EnableWindow(hflux, FALSE);
		EnableWindow(hluminisoty, FALSE);
		EnableWindow(hIncreamentF, FALSE);
		EnableWindow(hDecreamentF, FALSE);
		EnableWindow(hIncreamentL, FALSE);
		EnableWindow(hDecreamentL, FALSE);
		EnableWindow(hDistance, FALSE);
		EnableWindow(hPhysics, FALSE);
		EnableWindow(hCalculateLuminosity, FALSE);
		EnableWindow(hResetPhysics, FALSE);
		EnableWindow(hMaths, FALSE);
		EnableWindow(hRadius, FALSE);
		EnableWindow(hHeight, FALSE);
		EnableWindow(hCaculateConeVolume, FALSE);
		EnableWindow(hConeVolume, FALSE);
		EnableWindow(hResetMaths, FALSE);
		EnableWindow(hChemistry, FALSE);
		EnableWindow(hActivationEnergy, FALSE);
		EnableWindow(hUniGasConstant, FALSE);
		EnableWindow(hAbsuluteTemp, FALSE);
		EnableWindow(hRateConstant, FALSE);
		EnableWindow(hCalculateRateConstant, FALSE);
		EnableWindow(hResetChemistry, FALSE);
		EnableWindow(hSaveStateP, FALSE);
		EnableWindow(hSaveStateC, FALSE);
		EnableWindow(hSaveStateM, FALSE);
		break;
	default:
		break;
	}
	return(true);
}
