#include"pch.h"
#include"Hello.h"
#include"MyPage.h"

using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;;

void MyCallbackMethod(Windows::UI::Xaml::ApplicationInitializationCallbackParams^ params) 
{
	App^ app = ref new App();
}
int main(Array<String^>^ Args) 
{
	ApplicationInitializationCallback^ callback = ref new ApplicationInitializationCallback(MyCallbackMethod);
	Application::Start(callback);
	return(0);
}

// class function definition
App::App() 
{

}

void App::OnLaunched(Windows::ApplicationModel::Activation::LaunchActivatedEventArgs^ args) 
{
	Page^ page = ref new MyPage();
	Window::Current->Content = page;
	Window::Current->Activate();
}
