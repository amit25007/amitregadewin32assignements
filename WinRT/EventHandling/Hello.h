#pragma once

using namespace Platform;

ref class App sealed : public Windows::UI::Xaml::Application
{
public:
	App();
	virtual void OnLaunched(Windows::ApplicationModel::Activation::LaunchActivatedEventArgs^ args) override;
};
