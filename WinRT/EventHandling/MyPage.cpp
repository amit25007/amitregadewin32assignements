#include"pch.h"
#include"MyPage.h"
using namespace Windows::Foundation; // TypedEventHandler
using namespace Windows::UI::Xaml::Media; // SolidColorBrush

MyPage::MyPage() {
	Window::Current->CoreWindow->KeyDown += ref new TypedEventHandler<CoreWindow^, KeyEventArgs^>(this, &MyPage::OnKeyDown);

	Grid^ grid = ref new Grid();
	textblock = ref new TextBlock();
	textblock->Text = "Event Handling";
	textblock->FontFamily = ref new Windows::UI::Xaml::Media::FontFamily("Segoe UI");
	textblock->FontSize = 40;
	textblock->FontStyle = Windows::UI::Text::FontStyle::Oblique;
	textblock->FontWeight = Windows::UI::Text::FontWeights::Bold;
	textblock->Foreground = ref new SolidColorBrush(Windows::UI::Colors::Gold);
	textblock->HorizontalAlignment = Windows::UI::Xaml::HorizontalAlignment::Center;
	textblock->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Center;
	grid->Children->Append(textblock);

	Button^ button = ref new Button();
	button->Content = "Press Me";
	button->Width = 400;
	button->Height = 200;
	button->BorderThickness = 12;
	button->BorderBrush = ref new SolidColorBrush(Windows::UI::Colors::Gold);
	button->Foreground = ref new SolidColorBrush(Windows::UI::Colors::Red);
	button->FontFamily = ref new Windows::UI::Xaml::Media::FontFamily("Lucida Console");
	button->FontSize = 40;
	button->FontWeight = Windows::UI::Text::FontWeights::Bold;
	button->FontStyle = Windows::UI::Text::FontStyle::Normal;
	button->HorizontalAlignment = Windows::UI::Xaml::HorizontalAlignment::Center;
	button->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Bottom;

	button->Click += ref new RoutedEventHandler(this, &MyPage::OnButtonClick);

	grid->Children->Append(button);
	this->Content = grid;
}

void MyPage::OnKeyDown(CoreWindow^ sender, KeyEventArgs^ args) 
{
	textblock->Text = "Some Keyboard button is Pressed";
}

void MyPage::OnButtonClick(Object^ sender, RoutedEventArgs^ args) 
{
	textblock->Text = "Mousebutton is clicked";
}
