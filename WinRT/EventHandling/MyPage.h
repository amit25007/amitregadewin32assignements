#pragma once
using namespace Platform; // for Object
using namespace Windows::UI::Xaml; // for RoutedEventArgs
using namespace Windows::UI::Xaml::Controls; // for Page, Button
using namespace Windows::UI::Core; // for CoreWindow and KeyEventArgs
ref class MyPage sealed : public Page 
{
private:
	TextBlock^ textblock;
public:
	MyPage();
	void OnKeyDown(CoreWindow^ sender, KeyEventArgs^ args);
	void OnButtonClick(Object^ sender, RoutedEventArgs^ args);
};
