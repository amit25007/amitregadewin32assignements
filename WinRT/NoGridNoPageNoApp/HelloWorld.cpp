#include "pch.h";
#include "HelloWorld.h"
using namespace Platform;
using namespace Windows::UI::Xaml;
using namespace Windows::ApplicationModel::Activation;
using namespace Windows::UI::Xaml::Controls; 
using namespace Windows::UI::Xaml::Media;

void MyCallbackMethod(Windows::UI::Xaml::ApplicationInitializationCallback^ params)
{
	App^ app = ref new App();
}

int main(Array<String^>^ args)
{
	Application::Start(ref new ApplicationInitializationCallback([](ApplicationInitializationCallbackParams ^params)
	{
		App^ app = ref new App();
	}));
	/*
	ApplicationInitializationCallback^ callback = ref new ApplicationInitializationCallback(MyCallbackMethod);
	Application::Start(callback);
	return(0);
	*/
}

void App::OnLaunched(Windows::ApplicationModel::Activation::LaunchActivatedEventArgs^ args)
{
	Page^ page = ref new Page();
	Grid^ grid = ref new Grid();

	TextBlock^ textblock = ref new TextBlock();
	textblock->Text = "Hello World !!";
	textblock->FontFamily = ref new Windows::UI::Xaml::Media::FontFamily("Segoe UI");
	textblock->FontSize = 72;
	textblock->FontStyle = Windows::UI::Text::FontStyle::Oblique;
	textblock->FontWeight = Windows::UI::Text::FontWeights::Bold;
	textblock->Foreground = ref new SolidColorBrush(Windows::UI::Colors::Gold);
	textblock->HorizontalAlignment = Windows::UI::Xaml::HorizontalAlignment::Center;
	textblock->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Center;
	grid->Children->Append(textblock);
	page->Content = grid;
	Window::Current->Content = textblock;
	Window::Current->Activate();
}
