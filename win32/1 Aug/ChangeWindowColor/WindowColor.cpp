// Headers
#include <windows.h>

// global functions declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName, TEXT("My Application"), WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	//Message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//variables
	HDC hdc;
	static RECT rc;
	PAINTSTRUCT ps;
	HBRUSH Brush;
	static COLORREF color = RGB(128, 128, 128);
	static int i = 0;

	// code
	switch (iMsg)
	{
	case WM_PAINT:

		GetClientRect(hwnd, &rc);
		hdc = BeginPaint(hwnd, &ps);

		Brush = CreateSolidBrush(color);
		SelectObject(hdc, Brush);
		FillRect(hdc, &rc, Brush);
		DeleteObject(Brush);
		SetTextColor(hdc, RGB(0, 255, 0));
		SetBkColor(hdc, RGB(0, 0, 0));
		if (i == 0) {
			DrawText(hdc, TEXT("Press keys to change background color"), -1, &rc, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
			i++;
		}
		else if (i == -1) {
			DrawText(hdc, TEXT("This is default color if you press other keys than specified"), -1, &rc, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
			i = i + 2;
		}
		EndPaint(hwnd, &ps);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case 'R':
			color = RGB(255, 0, 0);
			break;
		case 'G':
			color = RGB(0, 255, 0);
			break;
		case 'B':
			color = RGB(0, 0, 255);
			break;
		case 'Y':
			color = RGB(255, 255, 0);
			break;
		case 'M':
			color = RGB(255, 0, 255);
			break;
		case 'C':
			color = RGB(0, 255, 255);
			break;
		case 'K':
			color = RGB(255, 255, 255);
			break;
		default:
			color = RGB(128, 128, 128);
			i = -1;
			break;
		}
		InvalidateRect(hwnd, &rc, false);
		SendMessage(hwnd, WM_PAINT, wParam, lParam);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
