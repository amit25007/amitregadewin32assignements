#include <Windows.h>
#include <math.h>
#include "Luminosity.h"

BOOL WINAPI DllMain(HMODULE hDll, DWORD dwReason, LPVOID lpReserved)
{
	//code
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	}
	return true;
}

//code using module defination file
extern "C" float LuminosityDistance(float Luminosity, float Flux)
{
	return(sqrt(Luminosity / (4 * 3.1415*Flux)));
}
