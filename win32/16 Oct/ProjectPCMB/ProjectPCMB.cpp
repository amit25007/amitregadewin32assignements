#include <windows.h>
#include <math.h>
#include <stdlib.h>
#include "ProjectPCMB.h"

//window variables
UINT width, height, startX, startY;
HWND hwndmodeless = NULL;

// global functions declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK DlgProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	startX = 40;
	startY = 40;
	width = 900;
	height = 600;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register above class
	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(szAppName, TEXT("My Application"), WS_OVERLAPPEDWINDOW, startX, startY, width, height, NULL, NULL, hInstance, NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (hwndmodeless == NULL || (IsDialogMessage(hwndmodeless, &msg) == 0))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	return static_cast<int>(msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, hMemdc;
	static HBITMAP hBitmap;
	PAINTSTRUCT ps;
	BITMAP bmp;
	HINSTANCE hInst = NULL;

	switch (msg)
	{
	case WM_CREATE:
		hInst = (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE);
		hBitmap = LoadBitmap(((LPCREATESTRUCT)lParam)->hInstance, MAKEINTRESOURCE(F1_BITMAP));
		break;
	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);
		hMemdc = CreateCompatibleDC(hdc);
		SelectObject(hMemdc, hBitmap);
		GetObject(hBitmap, sizeof(BITMAP), &bmp);
		StretchBlt(hdc, 0, 0, width, height, hMemdc, 0, 0, width, height, SRCCOPY);
		SelectObject(hMemdc, hBitmap);
		DeleteDC(hMemdc);
		EndPaint(hwnd, &ps);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_SPACE:
			hwndmodeless = CreateDialog(hInst, "MyDialog", hwnd, DlgProc);
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(WM_QUIT);
		break;
	default:
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}
	return 0;
}

BOOL CALLBACK DlgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	HWND hflux, hluminisoty, hdistance, hPhysics, hSave, hReset;
	FLOAT Luminosity=0.0, Flux=0.0, Distance;
	TCHAR sLuminosity[255], sFlux[255], sDistance[255];
	TCHAR ldistance[255];
	hflux = GetDlgItem(hwndmodeless, ID_FLUX);
	hluminisoty = GetDlgItem(hwndmodeless, ID_LUMINOSITY);
	hdistance = GetDlgItem(hwndmodeless, ID_DISTANCE);
	hPhysics = GetDlgItem(hwndmodeless, ID_LUMINOSITYDISTANCETEXT);
	hSave = GetDlgItem(hwndmodeless, ID_SAVE);
	hReset = GetDlgItem(hwndmodeless, ID_RESET);

	switch (msg)
	{
	case WM_INITDIALOG:
		EnableWindow(hflux, FALSE);
		EnableWindow(hluminisoty, FALSE);
		EnableWindow(hdistance, FALSE);
		EnableWindow(hPhysics, FALSE);
		EnableWindow(hSave, FALSE);
		EnableWindow(hReset, FALSE);
		return(TRUE);
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case ID_PHYSICS:
			EnableWindow(hflux, TRUE);
			EnableWindow(hluminisoty, TRUE);
			EnableWindow(hdistance, TRUE);
			EnableWindow(hPhysics, TRUE);
			EnableWindow(hSave, TRUE);
			EnableWindow(hReset, TRUE);
			break;
		case ID_CHEMISTRY:
			EnableWindow(hflux, FALSE);
			EnableWindow(hluminisoty, FALSE);
			EnableWindow(hdistance, FALSE);
			EnableWindow(hPhysics, FALSE);
			EnableWindow(hSave, FALSE);
			EnableWindow(hReset, FALSE);
			break;
		case ID_MATHS:
			EnableWindow(hflux, FALSE);
			EnableWindow(hluminisoty, FALSE);
			EnableWindow(hdistance, FALSE);
			EnableWindow(hPhysics, FALSE);
			EnableWindow(hSave, FALSE);
			EnableWindow(hReset, FALSE);
			break;
		case ID_BIOLOGY:
			EnableWindow(hflux, FALSE);
			EnableWindow(hluminisoty, FALSE);
			EnableWindow(hdistance, FALSE);
			EnableWindow(hPhysics, FALSE);
			EnableWindow(hSave, FALSE);
			EnableWindow(hReset, FALSE);
			break;
		case ID_INCREAMENT_LUMINOSITY:
			GetDlgItemText(hwndmodeless, ID_LUMINOSITY, sLuminosity, 255);
			Luminosity = atof(sLuminosity);
			Luminosity = Luminosity + 1.0;
			_gcvt_s(sLuminosity, Luminosity, 10);
			SetDlgItemText(hwndmodeless, ID_LUMINOSITY, sLuminosity);
			break;
		case ID_DECREAMENT_LUMINOSITY:
			GetDlgItemText(hwndmodeless, ID_LUMINOSITY, sLuminosity, 255);
			Luminosity = atof(sLuminosity);
			Luminosity = Luminosity - 1.0;
			_gcvt_s(sLuminosity, Luminosity, 10);
			SetDlgItemText(hwndmodeless, ID_LUMINOSITY, sLuminosity);
			break;
		case ID_INCREAMENT_FLUX:
			GetDlgItemText(hwndmodeless, ID_FLUX, sFlux, 255);
			Flux = atof(sFlux);
			Flux = Flux + 1.0;
			_gcvt_s(sFlux, Flux, 10);
			SetDlgItemText(hwndmodeless, ID_FLUX, sFlux);
			break;
		case ID_DECREAMENT_FLUX:
			GetDlgItemText(hwndmodeless, ID_FLUX, sFlux, 255);
			Flux = atof(sFlux);
			Flux = Flux - 1.0;
			_gcvt_s(sFlux, Flux, 10);
			SetDlgItemText(hwndmodeless, ID_FLUX, sFlux);
			break;
		case ID_SAVE:
			GetDlgItemText(hwndmodeless, ID_FLUX, sFlux, 255);
			GetDlgItemText(hwndmodeless, ID_LUMINOSITY, sLuminosity, 255);
			Flux = atof(sFlux);
			Luminosity = atof(sLuminosity);
			if (Flux <= 0 || Luminosity < 0)
			{
				SetDlgItemText(hwndmodeless, ID_DISTANCE, NULL);
				MessageBox(hwndmodeless, TEXT("Please enter positive integer values"), TEXT("Error"), MB_OK);
			}
			else
			{
				Distance = sqrt(Luminosity / (4 * 3.1415*Flux));
				_gcvt_s(ldistance, Distance, 10);
				SetDlgItemText(hwndmodeless, ID_DISTANCE, ldistance);
			}
			break;
		case ID_RESET:
			SetDlgItemText(hwndmodeless, ID_FLUX, NULL);
			SetDlgItemText(hwndmodeless, ID_LUMINOSITY, NULL);
			SetDlgItemText(hwndmodeless, ID_DISTANCE, NULL);
			break;
		case ID_EXIT:
			EndDialog(hwnd, 0);
			break;
		}
		return(TRUE);
	}
	return(FALSE);
}
