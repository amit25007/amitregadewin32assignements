#pragma once
#define F1_BITMAP 101
#define P_BITMAP 102
#define C_BITMAP 103
#define M_BITMAP 104
#define B_BITMAP 105
#define ID_PHYSICS 106
#define ID_CHEMISTRY 107
#define ID_MATHS 108
#define ID_BIOLOGY 109
#define ID_SAVE 110
#define ID_RESET 111
#define ID_EXIT 112
#define ID_LUMINOSITY 113
#define ID_FLUX 114
#define ID_DISTANCE 115
#define ID_LUMINOSITYTEXT 116
#define ID_FLUXTEXT 117
#define ID_DISTANCETEXT 118
#define ID_LUMINOSITYDISTANCETEXT 119
#define ID_INCREAMENT_LUMINOSITY 120
#define ID_DECREAMENT_LUMINOSITY 121
#define ID_INCREAMENT_FLUX 122
#define ID_DECREAMENT_FLUX 123
