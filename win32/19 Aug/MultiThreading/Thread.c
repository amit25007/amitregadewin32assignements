// Headers
#include <windows.h>
#include <tchar.h>
#include <math.h>

// global functions declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

DWORD WINAPI CreateThreadOne(LPVOID);
DWORD WINAPI CreateThreadTwo(LPVOID);

//global thread variables
static UINT SleepValue, SineFactor;

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName, TEXT("My Application"), WS_OVERLAPPEDWINDOW, 10, 10, 1200, 700, NULL, NULL, hInstance, NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	//Message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//paint vars
	HDC hdc;
	static RECT rc;
	PAINTSTRUCT ps;
	static UINT PaintCall=0;

	//Thread vars
	static HANDLE hThread1 = NULL;
	static HANDLE hThread2 = NULL;

	//lpvData = (LPVOID)GlobalAlloc(LPTR, 256);
	//TlsIndex = TlsAlloc();

	DWORD dwid1, dwid2;
	// code
	switch (iMsg)
	{
	case WM_CREATE:
		hThread1 = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)CreateThreadOne, (LPVOID)hwnd, CREATE_SUSPENDED, &dwid1);
		if (hThread1 == NULL) {
			MessageBox(hwnd, TEXT("Thread one creation failed"), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
		}
		hThread2 = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)CreateThreadTwo, (LPVOID)hwnd, CREATE_SUSPENDED, &dwid2);
		if (hThread2 == NULL) {
			MessageBox(hwnd, TEXT("Thread two creation failed"), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
		}
		break;
	case WM_PAINT:
		if (PaintCall==0)
		{
			hdc = BeginPaint(hwnd, &ps);
			GetClientRect(hwnd, &rc);
			SetTextColor(hdc, RGB(0, 255, 0));
			SetBkColor(hdc, RGB(0, 0, 0));
			DrawText(hdc, TEXT("Press spacebar to start threads"), -1, &rc, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
			EndPaint(hwnd, &ps);
		}
		else
		{
			SendMessage(hwnd, WM_KEYDOWN, VK_SPACE, lParam);
		}
		PaintCall++;
		break;
	case WM_LBUTTONDOWN:
		MessageBox(hwnd, TEXT("I am forked thread"), TEXT("Box"), MB_OK);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_SPACE:
			GetClientRect(hwnd, &rc);
			InvalidateRect(hwnd, &rc, TRUE);
			hdc = BeginPaint(hwnd, &ps);
			SetTextColor(hdc, RGB(128, 255, 255));
			SetBkColor(hdc, RGB(0, 0, 0));
			TextOut(hdc, 5, 10, TEXT("To increase speed : "), 20);
			TextOut(hdc, 5, 30, TEXT("To decrease speed : "), 20);
			TextOut(hdc, 5, 50, TEXT("For normal speed : "), 19);
			TextOut(hdc, 5, 70, TEXT("To increase wavelenght : "), 25);
			TextOut(hdc, 5, 90, TEXT("To decrease wavelenght : "), 25);
			SetTextColor(hdc, RGB(0, 128, 192));
			TextOut(hdc, 180, 10, TEXT("Press F"), 7);
			TextOut(hdc, 180, 30, TEXT("Press S"), 7);
			TextOut(hdc, 180, 50, TEXT("Press N"), 7);
			TextOut(hdc, 180, 70, TEXT("Up Arrow"), 8);
			TextOut(hdc, 180, 90, TEXT("Down Arrow"), 10);

			EndPaint(hwnd, &ps);
			ResumeThread(hThread1);
			ResumeThread(hThread2);
			break;
		case 'F':
			if (SleepValue==0)
			{
				SleepValue = 2;
			}
			SleepValue = abs(SleepValue - 2);
			break;
		case 'S':
			SleepValue = SleepValue + 2;
			break;
		case 'N':
			SleepValue = 10;
			break;
		case VK_UP:
			if (SineFactor==195)
			{
				SineFactor = 190;
			}
			SineFactor = (SineFactor + 5) % 200;
			//lpvData = &sineFactor;
			//TlsSetValue(TlsIndex, lpvData);
			break;
		case VK_DOWN:
			if (SineFactor == 0)
			{
				SineFactor = 5;
			}
			SineFactor = abs(SineFactor - 5);
			//lpvData = &sineFactor;
			//TlsSetValue(TlsIndex, lpvData);
			break;
		}
		break;
	case WM_DESTROY:
		CloseHandle(hThread1);
		CloseHandle(hThread2);
		PostQuitMessage(0);
		break;
	}
	//TlsFree(TlsIndex);
	//GlobalFree(lpvData);
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

DWORD WINAPI CreateThreadOne(LPVOID param)
{
	//static int lpvData1;
	
	HDC hdc;
	static RECT rc;
	rc.bottom = 600;
	rc.bottom = 255;
	rc.right = 1200;
	rc.top = 0;
	
	int i;
	long upperLimit= 2147483647;
	TCHAR str[255];
	HWND hwnd = (HWND)param;
	if (upperLimit > 32767)
	{
		long i;
	}
	else {
		int upperLimit;
	}

	//math variables
	double x, y, fun, radian;
	int RGB_R, RGB_G, RGB_B;
	SleepValue = 10;
	SineFactor = 75;
	hdc = GetDC(hwnd);
	SetBkColor(hdc, RGB(0, 0, 0));

	for (i = 0; i <= upperLimit; i++) {
		radian = (i % 360)*3.1415 / 180;
		fun = sin(radian);
		x = 255 + (i % 965);
		y = 200 + (fun * SineFactor);

		RGB_R = (0 + i) % 255;
		RGB_G = (128 + i) % 255;
		RGB_B = (255 - i) % 255;
		SetTextColor(hdc, RGB(RGB_R, RGB_G, RGB_B));
		TextOut(hdc, 10, 200, TEXT("Thread 1-> Increasing order input ="), 35);
		SetTextColor(hdc, RGB(0, 255, 0));
		wsprintf(str, TEXT("%d"), i);
		TextOut(hdc, (int)x, (int)y, str, (int)_tcslen(str));
		Sleep(SleepValue);
	}
	ReleaseDC(hwnd, hdc);
	return(0);
}

DWORD WINAPI CreateThreadTwo(LPVOID param)
{
	HDC hdc;
	int i, j;
	TCHAR str[255];
	long upperLimit = 2147483647;
	if (upperLimit > 32767)
	{
		long i, j;
	}
	else {
		int upperLimit;
	}
	
	//math variables
	double x, y, fun, radian;
	int RGB_R, RGB_G, RGB_B;
	SleepValue = 10;
	SineFactor = 75;

	hdc = GetDC((HWND)param);
	SetBkColor(hdc, RGB(0, 0, 0));

	for (i = upperLimit; i >= 0; i--) {
		j = upperLimit - i;
		radian = (j % 360)*3.1415 / 180;
		fun = sin(radian);
		x = 255 + (j % 965);
		y = 350 + (fun * SineFactor);

		RGB_R = (0 + i) % 255;
		RGB_G = (128 + i) % 255;
		RGB_B = (255 - i) % 255;
		SetTextColor(hdc, RGB(RGB_R, RGB_G, RGB_B));
		TextOut(hdc, 10, 350, TEXT("Thread 2-> Decreasing order input ="), 35);
		SetTextColor(hdc, RGB(0, 255, 0));
		wsprintf(str, TEXT("%d"), i);
		TextOut(hdc, (int)x, (int)y, str, (int)_tcslen(str));
		Sleep(SleepValue);
	}
	ReleaseDC((HWND)param, hdc);
	return(0);
}
